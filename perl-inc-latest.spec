Name:           perl-inc-latest
Epoch:          2
Version:        0.500
Release:        13
Summary:        Use modules bundled in inc/ if they are newer than installed ones
License:        Apache-2.0
URL:            https://metacpan.org/release/inc-latest
Source0:        https://cpan.metacpan.org/authors/id/D/DA/DAGOLDEN/inc-latest-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  make perl-generators perl-interpreter
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(strict) perl(warnings)
BuildRequires:  perl(File::Spec) perl(Test::More)
Requires:       perl(ExtUtils::Installed)

%description
The inc::latest module helps bootstrap configure-time dependencies for CPAN
distributions. These dependencies get bundled into the inc directory within
a distribution and are used by Makefile.PL or Build.PL.

%package_help

%prep
%autosetup -p1 -n inc-latest-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%license LICENSE
%{perl_vendorlib}/*

%files help
%doc Changes README
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 2:0.500-13
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Dec 3 2019 mengxian <mengxian@huawei.com> - 2:0.500-12
- Package init
